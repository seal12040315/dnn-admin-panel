<?php
namespace App\Library;

class Cpanel {
    public $header, $query;
    public function __construct($ip, $port, $user, $token, $subuser, $dbuser) {
        $this->header = "Authorization: whm $user:$token";
        $this->dbUser = $dbuser;
        $this->query = "https://$ip:$port/json-api/cpanel?cpanel_jsonapi_user=$subuser&cpanel_jsonapi_apiversion=2&";
    }

    private function requestCurl($query) {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => [$this->header],
            CURLOPT_URL => $this->query . $query
        ]);
        $result = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status == 200) {
            return $result;
        } else {
            return $status;
        }
        curl_close($curl);
    }

    public function addSubDomain($subDomain, $domain, $directory) {
        $query = "cpanel_jsonapi_module=SubDomain&cpanel_jsonapi_func=addsubdomain&domain=$subDomain&rootdomain=$domain&dir=%2F$directory&disallowdot=1";
        return $this->requestCurl($query);
    }

    public function addDatabase($dbName) {
        $query = "cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=createdb&db=$dbName";
        return $this->requestCurl($query);
    }

    public function addDbUserPrivileges($dbName) {
        $dbUser = $this->dbUser;
        $query = "cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=setdbuserprivileges&db=$dbName&dbuser=$dbUser&privileges=ALL PRIVILEGES";
        return $this->requestCurl($query);
    }

    public function extractSource($sourcePath, $dirPath) {
        $query = "cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=extract&sourcefiles=$sourcePath&destfiles=$dirPath&doubledecode=1&metadata=zip";
        return $this->requestCurl($query);
    }

    public function delSubDomain($subDomain) {
        $query = "cpanel_jsonapi_module=SubDomain&cpanel_json_api_func=delsubdomain&domain=$subDomain";
        return $this->requestCurl($query);
    }

    public function delSource($sourcePath) {
        $query = "cpanel_jsonapi_module=Fileman&cpanel_jsonapi_func=fileop&op=unlink&sourcefiles=$sourcePath&doubleddecode=1";
        return $this->requestCurl($query);
    }

    public function delDatabase($dbName) {
        $query = "cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=deletedb&db=$dbName";
        return $this->requestCurl($query);
    }

}