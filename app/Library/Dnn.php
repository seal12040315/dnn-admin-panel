<?php
namespace App\Library;

class Dnn {
    public $url;
    
    public function __construct($ip) {
        $this->url = "http://$ip";
    }

    public function requestCurl($endpoint, $data) {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $this->url . $endpoint,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($data)
        ]);
        $result = curl_exec($curl);
        if (curl_errno($curl)) {
            return NULL;
        } else {
            return $result;
        }
        curl_close($curl);
    }
}