<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Library\Cpanel;
use App\Library\Dnn;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create($data);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $data = $request->all();
        
        $cpanel = new Cpanel("162.214.75.102", "2087", "root", "NGXGPWKUOTU9Q3YD67UCVGJKR1O9RW5J", "lena", "lena_voice123");
        // subdomain creating.....
        $result = json_decode($cpanel->addSubDomain($data["username"], $_SERVER["HTTP_HOST"], "ownerpanels/" . $data["username"]), TRUE);
        if (isset($result["cpanelresult"]["error"])) {
            return response()->json(["error" => $result["cpanelresult"]["error"]]);
        }
        // extract source.....
        $result = json_decode($cpanel->extractSource($_SERVER["DOCUMENT_ROOT"] . "/public/saas.zip", $_SERVER["DOCUMENT_ROOT"] . "/ownerpanels/" . $data["username"]), TRUE);
        if (isset($result["cpanelresult"]["error"])) {
            return response()->json(["error" => $result["cpanelresult"]["error"]]); 
        }
        // create database.....
        $result = json_decode($cpanel->addDatabase("lena_" . $data["username"]), TRUE);
        if (isset($result["cpanelresult"]["error"])) {
            return response()->json(["error" => $result["cpanelresult"]["error"]]);
        }
        // assign user privileges into database....
        $result = json_decode($cpanel->addDbUserPrivileges("lena_" . $data["username"]), TRUE);
        if (isset($result["cpanelresult"]["error"])) {
            return response()->json(["error" => $result["cpanelresult"]["error"]]);
        }
        // sending owner info to dnn middleware....
        $dnn = new Dnn("44.229.170.39");
        $data["domain"] = $data["username"] . "." . $_SERVER["HTTP_HOST"];
        $result = json_decode($dnn->requestCurl("/owner/register", $data), TRUE);
        if (isset($result["error"])) {
            // delete created subdomain....
            $cpanel->delSubDomain($data['domain']);
            // delete extracted source....
            $cpanel->delSource('ownerpanels/' . $data['username']);
            // delete created database....
            $cpanel->delDatabase("lena_" . $data['username']);
            return response()->json(["error" => $result["error"]]);
        } else {
            $data["api_token"] = $result["token"];
            $data['password'] = Hash::make($data['password']);
            $user = $this->create($data);
            return response()->json(["user" => $user]);
        }
    }
}
