$(document).ready(function() {	
    $("#register_form").submit(function(e) {
        e.preventDefault();
        $.showLoading({name: 'line-scale'});  
        let data = {
            _token: $('meta[name="csrf-token"]').attr('content'),
            name: $("[name='name']").val(),
            username: $("[name='username']").val(),
            email: $("[name='email']").val(),
            password: $("[name='password']").val(),
            password_confirmation: $("[name='password_confirmation']").val()
        }
        if (data.name.trim() == "") {
            $.hideLoading();
            toastr.error("You must enter the name field.", "Warning");
        } else if (data.username.trim() == "") {
            $.hideLoading();
            toastr.error("You must enter the username field.", "Warning");
        } else if (/\s/g.test(data.username)) {
            $.hideLoading();
            toastr.error("You don't must contain white space in username field.", "Warning");
        } else if (data.password.trim() == "") {
            $.hideLoading();
            toast.error("You must enter the password field.", "Warning");
        } else if (data.password_confirmation.trim() == "") {
            $.hideLoading();
            toastr.error("You must enter the confirm field.", "Warning");
        } else if (data.password !== data.password_confirmation) {
            $.hideLoading();
            toastr.error("The password field should be same with the confirm password field.", "Warning");
        } else {
            $.ajax({
                url: `${baseUrl}/register`,
                type: "POST",
                data: data
            }).then(function(result) {
                $.hideLoading();
                if (result.error) {
                    toastr.error(result.error, "Warning");
                } else {
                    toastr.success("You have registered successfully.", "Success");
                    setTimeout(function() {
                        location.href = baseUrl + '/login'
                    }, 1500);
                }
            }, function(err) {
                $.hideLoading();
                var errors = JSON.parse(err.responseText);
                $.each(errors, function(key, val) {
                    toastr.error(val[0], "Warning");
                });
            });
        }
    })
});