<aside class="main-sidebar sidebar-dark-primary elevation-4 sidebar-no-expand">
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('images/logo.png') }}" alt="Admin Panel" class="brand-image img-circle" style="opacity: .8">
      <span class="brand-text font-weight-light">VoxNet</span>
    </a>
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ auth()->user()->photo != '' ? asset('uploads/users/' . auth()->user()->photo) : asset('images/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ url('admin/profile') }}" class="d-block">Saar Rozenbaum</a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent nav-legacy" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ url('admin/dashboard') }}" class="nav-link {{ Request::segment(2) == 'dashboard' ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/users') }}" class="nav-link {{ Request::segment(2) == 'users' ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Owners
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
  </aside>